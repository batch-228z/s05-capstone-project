console.log("Hello World!")

/*
1. Customer class:

email property - string

cart property - instance of Cart class

orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty


2. Product class:

name property - string

price property - number

isActive property - Boolean: defaults to true

archive() method - will set isActive to false if it is true to begin with

updatePrice() method - replaces product price with passed in numerical value


3. Cart class:

contents property - array of objects with structure: {product: instance of Product class, quantity: number}

totalAmount property - number

addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

showCartContents() method - logs the contents property in the console

updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

clearCartContents() method - empties the cart contents

computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/


// Code Here:

class Customer {
	constructor(email, cart, orders){
		this.email = email; 
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut(cart, orders){

			if (Cart != "") {
			this.orders.push(this.cart);
		return this
		}
	}
}

class Product {
	constructor(name, price, isActive){
		this.name = name;
		this.price = price;
		this.isActive = true;
			
	}

	archive(){
		if(this.isActive == true){
			this.isActive = false;
			return this
		}
	}

	updatePrice(newPrice) {
		this.price = newPrice
		return this
	}
}

class Cart {
	constructor(contents, totalAmount){
		this.contents = []
		this.totalAmount = 0;
	}

	addToCart(Product, quantity) {
		
		this.contents.push({product: Product, quantity: quantity})
		return this
	}

	showCartContents(){
		
		return this.contents
		return this
	}

	updateProductQuantity(product, newQuantity){

		this.quantity == newQuantity
		this.contents.push({product: Product, quantity: newQuantity})
		return this
	}

	clearCartContents(){
		this.contents = []
		this.totalAmount = 0
		return this
	}

	computeTotal(){
		
	}

}



//test statements
const john = new Customer('john@mail.com')

const prodA = new Product('soap', 9.99)
const prodB = new Product('shampoo', 12.99)
const prodC = new Product('toothbrush', 4.99)
const prodD = new Product('toothpaste', 14.99)

// const newProductPrice = new Product()



// john.cart.addToCart(prodA, 3)
// john.cart.addToCart(prodB, 2)

// john.cart.computeTotal()
// john.cart.updateProductQuantity('soap', 5)
// john.cart.showCartContents()
// john.cart.clearCartContents()
// john.cart.showCartContents()
// john.checkOut()

